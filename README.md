CarCar is a simple webapp designed to track a car dealership with services for it's inventory, sales, and service. Each service is broken out into it's own microservice, utilizing RESTful APIs to communicate with one another. The project also uses Docker containers.

It's built on a Django backend, React frontend, and uses a PostgreSQL database.
* Yaning - Service microservice
* Orion - Sales microservice

## Design
![](Project%20Design.png)
BC stand for Bounded Text

## Instructions to run project
1. When running docker for the first time use commands
***
        Clone the repo: ` git clone https://gitlab.com/thtguy00/project-beta `

        docker volume create beta-data ( this command create the data for docker)

        docker-compose build ( this command builds the containers for docker)

        docker-compose up ( this command runs the containers for the docker)

        docker-compose down ( this command stops the containers for the docker)

        Please wait until all the servers are running. Please be patient as this may take a while (~5 minutes or more) on the first time. It won't show anything out of the ordinary in the terminal - like it's working on something - so please be patient. If you're still having issues with viewing the page stop the servers with `control` + `c` and then restart them. 
        access the website at `localhost:3000`

        In docker, run migrations for service and sales api by using
                python manage.py makemigrations
                python manage.py migrate

2. Status instances need to be built on the Django admin (http://localhost:8080/admin/service_rest/status/) in order to be able to create a service appointment.
***
        id 1 = "Scheduled"
        id 2 = "Cancel"
        id 3 = "Finish"
## Ports/URL for microservices

1. Automobile (port 8100):
***
        For automobile list
                http://localhost:8100/api/automobiles/.
        For inidivudal, create,delete, and update automobile
                http://localhost:8100/api/automobiles/:vin/
                        ":vin" is replaced with the vin number of the vehicle.
        Example POST data
                {
                        "href": "/api/automobiles/1C3CC5FB2AN120174/",
                        "id": 1,
                        "color": "red",
                        "year": 2012,
                        "vin": "1C3CC5FB2AN120174",
                        "model": {
                                "href": "/api/models/1/",
                                "id": 1,
                                "name": "Sebring",
                                "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                                "manufacturer": {
                                        "href": "/api/manufacturers/1/",
                                        "id": 1,
                                        "name": "Chrysler"
                                }
                        }
                }

2. Vehicle (port 8100):
***
        For vehicle list
                http://localhost:8100/api/models/.
        For inidivudal, create,delete, and update vehicle
                http://localhost:8100/api/models/:id/
                        ":id" is replaced with the id number of the vehicle.
        Example POST data
                {
                        "href": "/api/models/1/",
                        "id": 1,
                        "name": "Sebring",
                        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
                        "manufacturer": {
                                "href": "/api/manufacturers/1/",
                                "id": 1,
                                "name": "Chrysler"
                        }
                }

3.  Manufacturer (port 8100):
***
        For manufacturer list
                http://localhost:8100/api/manufacturers/.
        For inidivudal, create,delete, and update manufacturer
                http://localhost:8100/api/models/:id/
                        ":id" is replaced with the id number of the manufacturer.
        Example POST data
                {
                        "href": "/api/manufacturers/1/",
                        "id": 1,
                        "name": "Chrysler"
                }

4. Service (port 8080):
***
        For service list
                http://localhost:8080/api/services/.
        For inidivudal, create, delete, and update service
                http://localhost:8080/api/services/:vin/
                        ":vin" is replaced with the vin number of the vehicle.
        Example POST data
                {
                        "vin": "1C3CC5FB2AN120154",
                        "customer_name": "John",
                        "date": "2022-10-26T18:38:24.747962+00:00",
                        "time": "2022-10-26T18:38:24.747972+00:00",
                        "reason": "",
                        "technician": {
                                "name": "Yaning",
                                "employee_number": 18
                        },
                        "status": {
                                "name": "Scheduled",
                                "id": 3
                        },
                        "vip": false
                }

## Inventory microservice
1. AutoForm
***
        - create a form that allows a person to enter automobile information when adding to inventory
                -color of the automobile
                -year of the automobile
                -vin of the automobile
                -model of the automobile
2. AutoList
***
        - get the list of automobile information
                -color of the automobile
                -year of the automobile
                -vin of the automobile
                -model of the automobile
                -manufacturer name of automobile
3. ManufactuerForm
***
        - create a form that allows a person to add name of manufacturer of automobiles

4. ManufacturerList
***
        - get a list of all the names of the automobile manufacturer

5. VehicleModelForm
***
        - create a form that allows a person to enter 
                - name of the vehicle
                - a picture of the vehicle
                - manufacturer name of the vehicle
        - when the form is submitted, the vehicle will be added to the vehicle list

6. VehicleModelList
***
        - shows a list of vehicles and it's informations

## Service Models
1. Status
***
        -This model create a Status class
        -Have an id and name properties
        -The purpose is to retrieve status of service appointments
        -The __str__ method returns what the user sees when they type in their browser address bar or run python manage .
        

2. Technician
***
        -This model create a Technician class
        -Have name and employee_number properties
        -The get_api_url method returns the URL for the list of technicians in reverse order by their ID, which is stored in self.pk.

3. AutomobileVO
***
        -This model create a automobile value object
        -Have vin, color, year and model properties
        -The get_api_url method is defined in this class and returns a URL that will return all of the automobiles with their respective vin numbers.

4. Service
***
        -This model create a Service class
        -Have vin, customer_name, date, time, reason, technician properties
        -The purpose of this class is to be used to represent all of the services in our system.
        -It is also set up so that there are options for how relationship associated with this class behaves when deleting records

## Service microservice 

1. TechnicianForm
***
        - create a form that allows a person to enter an automotive technician's name and employee number

2. ServiceForm
***
        - create a form that allows a service concierge to enter 
                -the VIN of the vehicle, 
                -the name of the person to whom the vehicle belongs, 
                -the date and time of the appointment, 
                -the assigned technician,  
                -a reason for the service appointment 
                        For example: "oil change" or "routine maintenance"
        -  When the form is submitted, the service appointment should be saved in the application  
        -  create a link in the navbar to get to the Enter a service appointment form.

3. ServiceList/ServiceList2
***
        - get the list of service appointment                       
                -VIN, 
                -customer name, 
                -date and time of the appointment, 
                -the assigned technician's name, 
                -the reason for the service
        - cancel/finish button
        - VIP treatment if the car was in inventory list   

4. ServiceHistoryForm
***
        - create a page that has an input that allows someone to type in the VIN.
                - on form submission,fetch all of the service appointments for an automobile with the VIN in the input.
                - show that list of service appointments to include 
                - create a link in the navbar to get to the page that shows the service history for a specific VIN.


## Features
***
        Create & List Manufacturers

        Create & List Vehicle Models

        Create & List Automobiles in the Inventory

        Create Technicians

        Create, List, Delete, and mark as complete Appointments

        The appointments list page will filter out any already completed appointments and also show if the car had been sold by us as indicated with the VIP section.
        
        Search for past Services by VIN
        
        Create sales person
        
        Create potential customers
        
        Create a sales record and list all sales
        
        Select a sales person from a drop down list and view detail of their sales

## Creating Data
***
        To start testing the website with some data, just go from left to right on the NavBar:
        
        Click on the Manufacturer tab in the NavBar, select the create on the drop down, and make a new manufacturer.
        
        Repeat for Vehicle Model
        
        Repeat for Automobile (to test if the vip section works make sure to use the same VIN number you used to create the automobile you entered)
        
        Repeat for Technician
        
        Repeat for Appointment
        
        Repeat for Salesperson
        
        Repeat for Potential Customer
        
        Repeat for Sales Record


## Navigating the Website
***
        Links are either at the top NavBar or in the relevant page. There are dropdown menus for the manufacturers, vehicle models, automobiles, appointments, and sales record on the NavBar. 
  
## Appointments
***
        Previously completed appointments won't show up in the appointments list.
        
        Mark appointments complete using the button on the entry 
        
        View all appointments for a certain vehicle (completed and uncompleted) by searching via VIN in the Service History Tab

## Service microservice
***
        For this service - microservice, poller was used to poll the automobile API from the inventory and then update/create and instance of an AutomobileVO on the Service - microservice. 

        Models were made for Automobile VO, Technicians, and Appointments. Technicians and Appointments both had full RESTful APIs (List, Details, Delete, Update, Create).

        VIP: Potentially, a customer model that would integrate with sales microservice.


## Sales microservice
***
        The sales microservice poll automobiles data from the inventory, which allows users to use, create, and update instance of AutomobileVO. 
