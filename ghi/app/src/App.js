import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
// import ManufacturerList from './Manufacturers'
// import ManufacturerForm from './ManufacturerForm';
// import AutoList from './Autos';
// import AutoForm from './AutoForm';
// import VehicleModelList from './VehicleModelList'
// import ModelForm from './ModelForm'
import CreateSalesPersonForm from './CreateSalesPersonForm';
import CreateCustomerForm from './CustomerForm';
// import AppointmentForm from './AppointmentForm';
// import AppointmentList from './AppointmentList';
// import SalesList from './SalesList';
import SaleRecordForm from './SaleRecordForm';
import SalesPersonSales from './SalesPersonSales';
import React from 'react';
import ServiceForm from './ServiceForm';
import ServicesList from './ServicesList';
import TechnicianForm from './TechnicianForm';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutoList from './AutoList';
import AutoForm from './AutoForm';
// import SalesList from './SalesList';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models" >
            <Route path ="" element={<VehicleModelList models={props.models} />} />
          </Route>
          <Route path="salesperson/" element={<CreateSalesPersonForm />} />
          <Route path="customer/" element={<CreateCustomerForm />} />
          <Route path="SaleRecordForm/" element={<SalesPersonSales />} />
          <Route path="SaleRecordForm/new/" element={<SaleRecordForm />} />
          <Route path="salesperson/history/" element={<SalesPersonSales/>} />
          <Route path="services/new/" element={<ServiceForm />} />
          <Route path="services_list/" element={<ServicesList/>} />
          <Route path="services_history/" element={<ServiceHistory />} />
          <Route path="technician/new/" element={<TechnicianForm />} />
          <Route path="manufacturers/" element={<ManufacturerList />} />
          <Route path="manufacturers/new/" element={<ManufacturerForm />} />
          <Route path="vehicle_models/" element={<VehicleModelList />} />
          <Route path="vehicle_models/new/" element={<VehicleModelForm />} />
          <Route path="automobiles/" element={<AutoList />} />
          <Route path="automobiles/new/" element={<AutoForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
